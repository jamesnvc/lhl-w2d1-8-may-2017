//
//  ViewController.m
//  NewAppDemo
//
//  Created by James Cash on 08-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "TouchingView.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *thingSwitch;

@property (weak,nonatomic) IBOutlet UITextField* thingField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    TouchingView *v1 = [[TouchingView alloc] initWithFrame:CGRectMake(15, 415, 200, 200)];
    [self.view addSubview:v1];
    v1.backgroundColor = [UIColor cyanColor];
    v1.niceName = @"cyan view";

    TouchingView *v2 = [[TouchingView alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
    v2.niceName = @"red view";
    [v1 addSubview:v2];
    v2.backgroundColor = [UIColor redColor];
//    v1.hidden = YES;
//    [self activateThingy:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)activateThingy:(id)sender {
    NSLog(@"Switch was flipped %@", sender);
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touch event on view controller");
    [self.thingField resignFirstResponder];
}

@end
