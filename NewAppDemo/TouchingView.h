//
//  TouchingView.h
//  NewAppDemo
//
//  Created by James Cash on 08-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchingView : UIView

@property (nonatomic,strong) NSString *niceName;

@end
