//
//  TouchingView.m
//  NewAppDemo
//
//  Created by James Cash on 08-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "TouchingView.h"

@implementation TouchingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"%@ got touch event", self.niceName);
    [self.nextResponder touchesBegan:touches withEvent:event];
}

@end
